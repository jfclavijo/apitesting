package com.apitesting.integration.springrestclient;

import com.apitesting.integration.springrestclient.apiary.model.Book;
import java.util.Arrays;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class Client {

  private static final String URL_API_BOOKS =
      "http://private-114e-booksapi.apiary-mock.com/books/";

  public static void main(String[] args) {
    RestTemplate restTemplate = new RestTemplate();
    //getBooks(restTemplate);
    //getBook(restTemplate);
    insertNewBook(restTemplate);

  }

  public static void getBooks(RestTemplate restTemplate){
    ResponseEntity<Book[]> response =
        restTemplate.getForEntity(URL_API_BOOKS, Book[].class);
    System.out.println();
    System.out.println("GET All StatusCode = " + response.getStatusCode());
    System.out.println("GET All Headers = " + response.getHeaders());
    System.out.println("GET Body (object list): ");
    Arrays.asList(response.getBody())
        .forEach(book -> System.out.println("--> " + book));
  }

  public static void getBook(RestTemplate restTemplate){
    ResponseEntity<Book> response2 =
        restTemplate.getForEntity(URL_API_BOOKS + "{id}", Book.class, 12L);

    System.out.println();
    System.out.println("GET StatusCode = " + response2.getStatusCode());
    System.out.println("GET Headers = " + response2.getHeaders());
    System.out.println("GET Body (object): " + response2.getBody());
  }

  public static void insertNewBook(RestTemplate restTemplate){
    Book bookToInsert = createBook(null, "New book title");
    ResponseEntity<Book> response =
        restTemplate.postForEntity(URL_API_BOOKS, bookToInsert, Book.class);
    System.out.println();
    System.out.println("POST executed");
    System.out.println("POST StatusCode = " + response.getStatusCode());
    System.out.println("POST Header location = " + response.getHeaders().getLocation());
  }

  private static Book createBook(Long id, String bookTitle) {
    Book newBook = new Book();
    newBook.setId(id);
    newBook.setTitle(bookTitle);
    return newBook;
  }

}
